#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

void promptin(char text[100]) {
    printf("Name of input file > ");
    fgets(text, 100, stdin);
}

void promptout(char text[100]) {
    printf("Name of output file > ");
    fgets(text, 100, stdin);
}

void loadfile(char *text1) {
    int x = strlen(text1);
    text1[x-1] = '\0';
    char c;
    //printf("%s\n", text1);
    //char temp[100];
    FILE *in = fopen(text1, "r");
    if (in) {
      char text2[100];
      promptout(text2);
      int y = strlen(text2);
      text2[y-1] = '\0';
      FILE *out = fopen(text2, "w");
      c = fgetc(in);
      fprintf(out, "%c", toupper(c));
      while((c = fgetc(in)) != EOF) {
        fprintf(out, "%c", toupper(c));
      }
      fclose(in);
      fclose(out);
    }
    else {
      printf("Input File not found\n");
    }
}


int main(int n, char *args[n]) {
    char text1[100];
    promptin(text1);
    loadfile(text1);
    //printf("%s\n", text2);
    return 0;
}
